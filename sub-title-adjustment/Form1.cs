﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sub_title_adjustment
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdjustSubTitle_Click(object sender, EventArgs e)
        {
            var linesFrom = File.ReadAllLines(txtFileFrom.Text, Encoding.UTF8).ToList();
            var linesTo = File.ReadAllLines(txtFileTo.Text, Encoding.UTF8).ToList();
            var linesOutPut = new List<string>();
            var lineToIndex = 0;
            var index = 0;
            for (int i = 0; i < linesFrom.Count; i++)
            {
                if (linesFrom[i].Contains("-->"))
                {
                    lineToIndex = GetLineToIndex(linesTo, lineToIndex);
                    if (lineToIndex < 0)
                    {
                        break;
                    }
                    index++;
                    linesOutPut.Add(index.ToString());
                    linesOutPut.Add(linesFrom[i]);
                    linesOutPut.Add(linesTo[lineToIndex] + "\r\n");
                }
            }

            File.WriteAllLines(txtFileOutput.Text, linesOutPut);
            MessageBox.Show("Success");
        }

        private int GetLineToIndex(List<string> linesTo, int indexStart)
        {
            for (int i = indexStart + 1; i < linesTo.Count; i++)
            {
                if (linesTo[i].Contains("-->"))
                {
                    return i + 1;
                }
            }
            return -1;
        }

    }
}
