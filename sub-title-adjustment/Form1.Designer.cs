﻿namespace sub_title_adjustment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFileFrom = new System.Windows.Forms.TextBox();
            this.txtFileTo = new System.Windows.Forms.TextBox();
            this.txtFileOutput = new System.Windows.Forms.TextBox();
            this.btnAdjustSubTitle = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtFileFrom
            // 
            this.txtFileFrom.Location = new System.Drawing.Point(58, 54);
            this.txtFileFrom.Name = "txtFileFrom";
            this.txtFileFrom.Size = new System.Drawing.Size(845, 20);
            this.txtFileFrom.TabIndex = 0;
            this.txtFileFrom.Text = "D:\\Planet Earth\\Planet.Earth.01.From.Pole.to.Pole.2006.1080p.HDDVD.x264-AJP.eng.s" +
    "rt";
            // 
            // txtFileTo
            // 
            this.txtFileTo.Location = new System.Drawing.Point(58, 90);
            this.txtFileTo.Name = "txtFileTo";
            this.txtFileTo.Size = new System.Drawing.Size(845, 20);
            this.txtFileTo.TabIndex = 1;
            this.txtFileTo.Text = "D:\\Planet Earth\\Planet.Earth.01.From.Pole.to.Pole.2006.1080p.HDDVD.x264-AJP-old.s" +
    "rt";
            // 
            // txtFileOutput
            // 
            this.txtFileOutput.Location = new System.Drawing.Point(58, 125);
            this.txtFileOutput.Name = "txtFileOutput";
            this.txtFileOutput.Size = new System.Drawing.Size(845, 20);
            this.txtFileOutput.TabIndex = 2;
            this.txtFileOutput.Text = "D:\\Planet Earth\\Planet.Earth.01.From.Pole.to.Pole.2006.1080p.HDDVD.x264-AJP_New.s" +
    "rt";
            // 
            // btnAdjustSubTitle
            // 
            this.btnAdjustSubTitle.Location = new System.Drawing.Point(58, 165);
            this.btnAdjustSubTitle.Name = "btnAdjustSubTitle";
            this.btnAdjustSubTitle.Size = new System.Drawing.Size(145, 23);
            this.btnAdjustSubTitle.TabIndex = 3;
            this.btnAdjustSubTitle.Text = "Adjust";
            this.btnAdjustSubTitle.UseVisualStyleBackColor = true;
            this.btnAdjustSubTitle.Click += new System.EventHandler(this.btnAdjustSubTitle_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 243);
            this.Controls.Add(this.btnAdjustSubTitle);
            this.Controls.Add(this.txtFileOutput);
            this.Controls.Add(this.txtFileTo);
            this.Controls.Add(this.txtFileFrom);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFileFrom;
        private System.Windows.Forms.TextBox txtFileTo;
        private System.Windows.Forms.TextBox txtFileOutput;
        private System.Windows.Forms.Button btnAdjustSubTitle;
    }
}

